package com.smess.basemvpandroidarchitecture.workers;

/**
 * Created by samy.messara
 */

public interface BaseWorkerCallback {
    void onWorkerError(Throwable error, String tag);
    void onTokenInvalid();
}
