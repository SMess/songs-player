package com.smess.basemvpandroidarchitecture.workers.di

import com.smess.basemvpandroidarchitecture.executor.di.ExecutorModule
import com.smess.basemvpandroidarchitecture.workers.AbstractWorker
import dagger.Component
import javax.inject.Singleton

/**
 * Created by samy.messara
 */

@Singleton
@Component(modules = [(ExecutorModule::class)])
interface WorkerComponent {
    fun inject(interactor: AbstractWorker)
}
