package com.smess.basemvpandroidarchitecture.ui

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment

/**
 * Created by samy.messara
 */
abstract class BaseFragment<T : MVPContract.PresenterContract> : Fragment(), MVPContract.ViewContract {

    /**
     * Presenter bound to this view (extends PresenterContract)
     */
    protected lateinit var presenter: T

    init {
        getPresenterInstance()
    }

    /**
     * Getter on the view's Presenter
     *
     * @return a bounded Presenter that extends MVPPresenter
     */
    fun getPresenterInstance(): T {
        presenter = createPresenter()
        return presenter
    }


    /**
     * Allow to expose the view's Presenter
     * For example:
     * protected MyPresenter createPresenter() {
     * return new MyPresenter(this);
     * }
     *
     * @return the Presenter we want to bind to this view (must extends MVPPresenter)
     */
    protected abstract fun createPresenter(): T

    /**
     * Allow to give a tag to this view.
     * Mostly used for meta info or to log
     *
     * @return a String to tag this view
     */
    protected abstract fun assignTag(): String

    /**
     * Bind its Presenter's onCreate.
     */

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        getPresenterInstance().onCreate(savedInstanceState)
    }

    /**
     * Bind its Presenter's onStart
     */
    override fun onStart() {
        super.onStart()
        getPresenterInstance().onStart()
    }

    /**
     * Bind its Presenter's onResume
     */
    override fun onResume() {
        super.onResume()
        getPresenterInstance().onResume()
    }

    /**
     * Bind its Presenter's onPause
     */
    override fun onPause() {
        getPresenterInstance().onPause()
        super.onPause()
    }

    /**
     * Bind its Presenter's onStop
     */
    override fun onStop() {
        getPresenterInstance().onStop()
        super.onStop()
    }


    /**
     * Bind its Presenter's onDestroy
     */
    override fun onDestroyView() {
        getPresenterInstance().onDestroy()
        super.onDestroyView()
    }


    override fun getViewAppContext(): Context {
        return context!!.applicationContext
    }
}
