package com.smess.basemvpandroidarchitecture.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

/**
 * Created by samy.messara
 */

abstract class BaseActivity<T : MVPContract.PresenterContract> : AppCompatActivity(), MVPContract.ViewContract {

    /**
     * Presenter bound to this view (extends PresenterContract)
     */
    protected lateinit var presenter: T

    init {
        getPresenterInstance()
    }

    /**
     * Get the application context
     */
    override fun getViewAppContext(): Context {
        return super.getApplicationContext()
    }

    /**
     * Bound its Presenter's onCreate
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getPresenterInstance().onCreate(savedInstanceState)
        getPresenterInstance().setIntent(intent)
    }

    /**
     * Bound its Presenter's onStart
     */
    override fun onStart() {
        super.onStart()
        getPresenterInstance().onStart()
    }

    /**
     * Bound its Presenter's onResume
     */
    override fun onResume() {
        super.onResume()
        getPresenterInstance().onResume()
    }

    /**
     * Bound its Presenter's onNewIntent
     */
    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        getPresenterInstance().onNewIntent(intent)
    }

    /**
     * Bound its Presenter's onPause
     */
    override fun onPause() {
        getPresenterInstance().onPause()
        super.onPause()
    }

    /**
     * Bound its Presenter's onStop
     */
    override fun onStop() {
        super.onStop()
        getPresenterInstance().onStop()
    }

    /**
     * Bound its Presenter's onDestroy
     */
    override fun onDestroy() {
        getPresenterInstance().onDestroy()
        super.onDestroy()
    }


    fun getPresenterInstance(): T {
        presenter = createPresenter()
        return presenter
    }

    abstract fun createPresenter(): T
}