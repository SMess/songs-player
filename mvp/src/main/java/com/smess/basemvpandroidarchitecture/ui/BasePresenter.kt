package com.smess.basemvpandroidarchitecture.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle

/**
 * Created by samy.messara
 */

abstract class BasePresenter<T : MVPContract.ViewContract>(protected var view: T) : MVPContract.PresenterContract {

    protected lateinit var appContext: Context

    /**
     * Bound to its view's onViewCreated
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        appContext = view.getViewAppContext()
    }

    /**
     * Bound to its view's onStart
     */
    override fun onStart() {
    }

    /**
     * Bound to its view's onNewIntent
     */
    override fun onNewIntent(intent: Intent) {
    }

    /**
     * Bound to its view's onPause
     */
    override fun onPause() {
    }

    /**
     * Bound to its view's onResume
     */
    override fun onResume() {
    }

    /**
     * Bound to its view's onStop
     */
    override fun onStop() {
    }

    /**
     * Bound to its view's onDestroyView
     */
    override fun onDestroy() {
    }

    /**
     * Bound to its view's setBundle
     */
    override fun setBundle(arguments: Bundle?) {}

    /**
     * Bound to its view's setIntent
     */
    override fun setIntent(intent: Intent?) {}

}