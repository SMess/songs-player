package com.smess.basemvpandroidarchitecture.executor.di

import com.smess.basemvpandroidarchitecture.executor.Executor
import com.smess.basemvpandroidarchitecture.executor.MainThread
import com.smess.basemvpandroidarchitecture.executor.impl.MainThreadImpl
import com.smess.basemvpandroidarchitecture.executor.impl.ThreadExecutor
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
* Created by samy.messara
*/

@Module
class ExecutorModule {

    @Provides
    @Singleton
    fun provideExecutor(): Executor? {
        return ThreadExecutor.instance
    }

    @Provides
    @Singleton
    fun provideMainThread(): MainThread {
        return MainThreadImpl.instance
    }

}
