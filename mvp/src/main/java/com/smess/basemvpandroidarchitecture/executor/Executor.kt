package com.smess.basemvpandroidarchitecture.executor

import com.smess.basemvpandroidarchitecture.workers.AbstractWorker


/**
 * This executor is responsible for running interactors on background threads.
 *
 * Created by samy.messara
 */

interface Executor {

    /**
     * This method should call the interactor's run method and thus start the interactor. This should be called
     * on a background thread as interactors might do lengthy operations.
     *
     * @param interactor The interactor to run.
     */
    fun execute(interactor: AbstractWorker)
}