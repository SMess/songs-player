package com.smess.songsplayer.services.search

import android.os.AsyncTask
import android.util.Log
import com.google.gson.Gson
import com.smess.songsplayer.BuildConfig
import com.smess.songsplayer.domain.models.ItunesSearch
import com.smess.songsplayer.domain.workers.search.SearchWorkerCallback
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

/**
 * Created by samy.messara
 */

class SearchService(var callback: SearchWorkerCallback, var tag: String) : AsyncTask<String, String, String>() {


    override fun onPreExecute() {
        // Before doInBackground
    }

    override fun doInBackground(vararg terms: String?): String {
        var urlConnection: HttpURLConnection? = null
        var result = ""

        try {
            val url = URL(BuildConfig.ITUNES_SERVICE_BASE_ULR + "search?term=" + terms[0])

            urlConnection = url.openConnection() as HttpURLConnection
            urlConnection.connectTimeout = 15000
            urlConnection.readTimeout = 15000

            result = streamToString(urlConnection.inputStream)

            publishProgress(result)
        } catch (ex: Exception) {

        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect()
            }
        }

        return result
    }

    override fun onPostExecute(result: String?) {
        // Done
        Log.d("SearchService OnPEx: ", result)

        if (result != null) {
            try {
                var itunesSearch: ItunesSearch = Gson().fromJson(result, ItunesSearch::class.java)
                callback.onSearchSuccess(itunesSearch, tag)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else {
            var error = Throwable("resul null")
            callback.onWorkerError(error, tag)
        }
    }
}

fun streamToString(inputStream: InputStream): String {

    val bufferReader = BufferedReader(InputStreamReader(inputStream))
    var line: String
    var result = ""

    try {
        do {
            line = bufferReader.readLine()
            if (line != null) {
                result += line
            }
        } while (line != null)
        inputStream.close()
    } catch (ex: Exception) {

    }

    return result
}

