package com.smess.songsplayer.scenes.main

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.smess.basemvpandroidarchitecture.ui.BasePresenter
import com.smess.songsplayer.R
import com.smess.songsplayer.domain.converters.ItunesFeedToSong
import com.smess.songsplayer.domain.converters.ItunesSearchToSong
import com.smess.songsplayer.domain.models.ItunesFeed
import com.smess.songsplayer.domain.models.ItunesSearch
import com.smess.songsplayer.domain.models.Song
import com.smess.songsplayer.domain.workers.feed.FeedWorker
import com.smess.songsplayer.domain.workers.feed.FeedWorkerCallback
import com.smess.songsplayer.domain.workers.search.SearchWorker
import com.smess.songsplayer.domain.workers.search.SearchWorkerCallback
import com.smess.songsplayer.scenes.main.adapter.ListSongsAdapter
import com.smess.songsplayer.utils.Utils
import java.util.*


class MainPresenter(viewContract: MainContract.ViewContract) : BasePresenter<MainContract.ViewContract>(viewContract),
        MainContract.PresenterContract, FeedWorkerCallback, SearchWorkerCallback {

    companion object {
        private val TAG = MainPresenter::class.java.name
        private val TAG_GET_TOP_CHARTS_FROM_REMOTE = TAG + "_get_top_charts_from_remote"
        private val TAG_SEARCH_WITH_TERMS_FROM_REMOTE = TAG + "_search_with_terms_from_remote"

        private const val VOICE_SEARCH_ERROR_PERMISSION_DENIED = 130
    }

    private var isRefresh: Boolean = false
    private var searchTerms = ""
    private var displayedFeed: ItunesFeed? = null
    private var displayedTermsSearch: ItunesSearch? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
    }

    override fun fetchTopCharts() {
        Log.d(TAG, "fetchTopCharts")
        if (Utils.isNetworkAvailable(view.getContext())) {

            if (!isRefresh) {
                view.showProgress()
            }

            FeedWorker(Locale.getDefault().country, this, TAG_GET_TOP_CHARTS_FROM_REMOTE).execute()
        } else {
            view.showSnackBarMessage(view.getContext().getString(R.string.no_network_available_error_message))
            view.showNoDataLayout()
            isRefresh = false
            view.stopRefreshing()
            view.hideProgress()
            view.hideSongsList()
        }
    }

    override fun onFetchTopChartsSuccess(itunesFeed: ItunesFeed, tag: String) {
        Log.d(tag, "onFetchTopChartsSuccess")
        updateMainViewContent(view.getContext().getString(R.string.title_top_charts), ItunesFeedToSong.feedToSongsList(itunesFeed))
        updateMainViewState()
    }

    fun updateMainViewState(){
        //Update the main view stats
        view.showSongsList()
        if (!isRefresh) {
            view.hideProgress()
        } else {
            view.stopRefreshing()
        }
        view.hideNoDataLayout()
        isRefresh = false
    }

    override fun onRefresh() {
        //On swipe to refresh data
        isRefresh = true
        if(view.getSearchViewContent().isEmpty()) {
            fetchTopCharts()
        }else {
            searchWithTerms(view.getSearchViewContent())
        }
    }

    override fun onSearchTextChange(searchTerms: CharSequence) {
        if(searchTerms.isEmpty()){
            view.updateSearchRightButtonDrawable(R.drawable.ic_microphone_grey600_24dp)
        }else {
            view.updateSearchRightButtonDrawable(R.drawable.ic_close_grey600_24dp)
        }
    }

    override fun onSearchSubmitted(terms: String): Boolean {
        Log.d(TAG, "onSearchSubmitted")
        if (Utils.isNetworkAvailable(view.getContext())) {

            if (!isRefresh) {
                view.showProgress()
            }

            if (terms.isEmpty()) {
                fetchTopCharts()
            } else {
                searchWithTerms(terms)
            }

        } else {
            view.showSnackBarMessage(view.getContext().getString(R.string.no_network_available_error_message))
            view.showNoDataLayout()
            isRefresh = false
            view.stopRefreshing()
            view.hideProgress()
            view.hideSongsList()
        }
        return true
    }

    private fun searchWithTerms(terms: String){
        searchTerms = terms
        SearchWorker(terms.replace("\\s".toRegex(), "+"), this, TAG_SEARCH_WITH_TERMS_FROM_REMOTE).execute()
    }

    override fun onSearchSuccess(itunesSearch: ItunesSearch, tag: String) {
        Log.d(tag, "onSearchSuccess")
        updateMainViewContent(String.format(Locale.getDefault(), view.getContext().getString(R.string.title_search_with_terms), searchTerms), ItunesSearchToSong.searchToSongsList(itunesSearch))
        updateMainViewState()
    }

    private fun updateMainViewContent(title:String, songList: ArrayList<Song>){
        var adapter = ListSongsAdapter(title, songList,
                listenerItemClick = {
                    //go to detail view
                    view.goToPreviewActivity(it.id, songList)
                })
        //Ask the view to update rv content
        view.refreshSongsListContent(adapter)
    }

    override fun onClickFabScrollTop() {
        view.smoothScrollToTop()
        view.hideFab()
    }

    override fun onScrollStateChanged(recyclerView: RecyclerView) {
        if (recyclerView.canScrollVertically(-1)) {
            view.showFab()
        } else {
            view.hideFab()
        }
    }

    override fun onClickSearchRightAction(searchText: String) {
        if(searchText.isEmpty()){
            view.startVoiceCapture()
        }else {
            view.clearMainSearchView()
            fetchTopCharts()
        }
    }

    override fun onClickSearchLeftAction(searchText: String) {
        onSearchSubmitted(searchText)
    }

    override fun onVoiceSearchError(code: Int) {
        Log.d(TAG, "VOICE_SEARCH_ERROR: $code")
        when (code) {
            VOICE_SEARCH_ERROR_PERMISSION_DENIED -> {
                val builder = AlertDialog.Builder(view.getContext())
                builder.setTitle(view.getContext().getString(R.string.title_popup_permission_denied))
                builder.setMessage(view.getContext().getString(R.string.body_popup_permission_denied))
                builder.setPositiveButton(android.R.string.ok) { dialogInterface, i -> dialogInterface.dismiss() }
                builder.show()
            }
        }
    }

    //Handle errors
    override fun onWorkerError(error: Throwable?, tag: String?) {
        Log.e(tag, "onWorkerError : " + error?.message)

        when {
            TAG_GET_TOP_CHARTS_FROM_REMOTE == tag -> {
                handleGenericError()
            }
            TAG_SEARCH_WITH_TERMS_FROM_REMOTE == tag -> {
                handleGenericError()
            }
        }
    }

    private fun handleGenericError(){
        //handle errors
        isRefresh = false
        view.stopRefreshing()
        view.hideProgress()
        view.hideSongsList()
        view.showNoDataLayout()
        view.showSnackBarMessage(view.getContext().getString(R.string.default_error_message))
    }

    override fun onTokenInvalid() {
        //nothing here for now
    }
}