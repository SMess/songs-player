package com.smess.songsplayer.scenes.main


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Parcelable
import android.speech.RecognizerIntent
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import com.jakewharton.rxbinding2.widget.textChanges
import com.smess.basemvpandroidarchitecture.ui.BaseActivity
import com.smess.songsplayer.R
import com.smess.songsplayer.domain.models.Song
import com.smess.songsplayer.scenes.main.adapter.ListSongsAdapter
import com.smess.songsplayer.scenes.preview.PreviewActivity
import com.smess.songsplayer.utils.Utils
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : BaseActivity<MainContract.PresenterContract>(), MainContract.ViewContract {

    companion object {
        private val TAG = MainActivity::class.java.simpleName
        private const val LIST_STATE_KEY = "recycler_list_state"
        private const val FAB_STATE_KEY = "fab_list_state"
        private const val REQUEST_CODE = 1
        private const val REQUEST_CODE_PERMISSION_RECORD_AUDIO = 777
        private const val VOICE_SEARCH_ERROR_PERMISSION_DENIED = 130
    }

    private var listState: Parcelable? = null
    private var layoutManager: LinearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)


    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        bindClicks()

        //Try to load songs data on activity create
        presenter.fetchTopCharts()

        swipeRefreshMainLayout.setOnRefreshListener(presenter)

        mainSearchView.textChanges()
                .subscribe({
                    Log.d("MainActivity", it.toString())
                    presenter.onSearchTextChange(it)
                }, {
                    //todo : Handle error cases
                    Log.e("MainActivity", it.toString())
                })
        mainSearchView.setOnEditorActionListener(TextView.OnEditorActionListener { textView, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                presenter.onSearchSubmitted(mainSearchView.text.toString())

                //Close the keyboard
                val imm = textView.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(textView.windowToken, 0)
            }
            true
        })

        //Add scrollListener to rv to handle Fab visibility
        rvSongsList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                presenter.onScrollStateChanged(recyclerView)
            }
        })

    }


    /**
     * All on click listeners will be regrouped here  (Call it right after setContentView)
     */
    private fun bindClicks() {
        fabScrollTop.setOnClickListener {
            presenter.onClickFabScrollTop()
        }

        ivSearchRightAction.setOnClickListener {
            presenter.onClickSearchRightAction(mainSearchView.text.toString())
        }

        ivSearchLeftAction.setOnClickListener {
            presenter.onClickSearchLeftAction(mainSearchView.text.toString())
        }
    }

    override fun getContext(): Context {
        return this
    }

    override fun getSearchViewContent(): String {
        return mainSearchView.text.toString()
    }

    override fun createPresenter(): MainContract.PresenterContract {
        return MainPresenter(this)
    }

    override fun refreshSongsListContent(adapter: ListSongsAdapter) {
        rvSongsList.layoutManager = layoutManager
        rvSongsList.adapter = adapter
    }


    override fun showProgress() {
        //show progressBar
        llProgressContainer.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        //hide progressBar
        llProgressContainer.visibility = View.GONE
    }

    override fun stopRefreshing() {
        swipeRefreshMainLayout.isRefreshing = false
    }

    override fun showNoDataLayout() {
        tvNoData.visibility = View.VISIBLE
    }

    override fun hideNoDataLayout() {
        tvNoData.visibility = View.GONE
    }

    override fun hideSongsList() {
        rvSongsList.visibility = View.GONE
    }

    override fun showSongsList() {
        rvSongsList.visibility = View.VISIBLE
    }

    override fun showSnackBarMessage(message: String) {
        Utils.displayErrorSnackbar(this, clMainContainer, message, getString(R.string.snackbar_button_ok))
    }


    override fun onSaveInstanceState(state: Bundle) {
        // Save  states
        listState = this.layoutManager.onSaveInstanceState()
        state.putParcelable(LIST_STATE_KEY, listState)
        state.putInt(FAB_STATE_KEY, fabScrollTop.visibility)

        super.onSaveInstanceState(state)
    }

    override fun onRestoreInstanceState(state: Bundle?) {
        super.onRestoreInstanceState(state)
        // Retrieve saved states
        if (state != null) {
            listState = state.getParcelable(LIST_STATE_KEY)
            fabScrollTop.visibility = state.getInt(FAB_STATE_KEY)
        }
    }

    override fun onResume() {
        super.onResume()
        if (listState != null) {
            this.layoutManager.onRestoreInstanceState(listState)
        }
    }

    override fun hideFab() {
        fabScrollTop.hide()
    }

    override fun showFab() {
        fabScrollTop.show()
    }

    override fun smoothScrollToTop() {
        rvSongsList.smoothScrollToPosition(0)
    }

    override fun startVoiceCapture() {
        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {

            // No explanation needed, we can request the permission.
            startRequestPermissions(arrayOf(Manifest.permission.RECORD_AUDIO), REQUEST_CODE_PERMISSION_RECORD_AUDIO)
            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
        } else {
            // Permission has already been granted
            val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
            intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.title_speak_prompt))
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
            intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, REQUEST_CODE)
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.US)
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, Locale.US)
            intent.putExtra(RecognizerIntent.EXTRA_ONLY_RETURN_LANGUAGE_PREFERENCE, Locale.US)
            startActivityForResult(intent, REQUEST_CODE)
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_CODE_PERMISSION_RECORD_AUDIO && grantResults.isNotEmpty() && permissions.isNotEmpty()) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && Manifest.permission.RECORD_AUDIO == permissions[0]) {
                startVoiceCapture()
            } else {
                presenter.onVoiceSearchError(VOICE_SEARCH_ERROR_PERMISSION_DENIED)
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultcode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultcode, intent)
        val speech: ArrayList<String>
        if (resultcode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE) {
                speech = intent?.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS) as ArrayList<String>
                mainSearchView.setText(speech[0])
            }
        }
    }

    override fun startRequestPermissions(permsList: Array<String>, requestCode: Int) {
        ActivityCompat.requestPermissions(this, permsList, requestCode)
    }

    override fun clearMainSearchView() {
        mainSearchView.text.clear()
    }

    override fun updateSearchRightButtonDrawable(resId: Int) {
        ivSearchRightAction.setImageDrawable(resources.getDrawable(resId, theme))
    }

    override fun goToPreviewActivity(songPosition: Int, songList: ArrayList<Song>) {
        val intent = Intent(this, PreviewActivity::class.java)
        intent.putExtra(PreviewActivity.ARG_SONG_POSITION, songPosition)
        intent.putExtra(PreviewActivity.ARG_SONG_LIST, songList)
        startActivity(intent)
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
    }
}
