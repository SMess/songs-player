package com.smess.songsplayer.scenes.main.adapter

import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.smess.songsplayer.R
import com.smess.songsplayer.domain.models.Song
import kotlinx.android.synthetic.main.item_header_layout.view.*
import kotlinx.android.synthetic.main.item_song_layout.view.*
import java.util.*


/**
 * Created by samy.messara
 */
class ListSongsAdapter(private val title: String, private val items: ArrayList<Song>, private val listenerItemClick: (Song) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TYPE_HEADER = 0
    private val TYPE_ITEM = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == TYPE_ITEM) {
            //inflate your layout and pass it to view holder
            var inflatedView = LayoutInflater.from(parent.context).inflate(R.layout.item_song_layout, parent, false)
            return ViewHolder(inflatedView)
        } else if (viewType == TYPE_HEADER) {
            //inflate your layout and pass it to view holder
            var inflatedView = LayoutInflater.from(parent.context).inflate(R.layout.item_header_layout, parent, false)
            return HeaderViewHoder(inflatedView)
        }

        throw RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly")
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int){
        if (holder is ViewHolder) {
            //cast holder to VHItem and set data
            holder.bind(getItem(position), listenerItemClick)
        } else if (holder is HeaderViewHoder) {
            //cast holder to VHHeader and set data for header.
            holder.bind(title)
        }
    }

    override fun getItemCount() = items.size + 1

    override fun getItemViewType(position: Int): Int {
        if (isPositionHeader(position))
            return TYPE_HEADER

        return TYPE_ITEM
    }

    private fun getItem(position: Int): Song {
        return items[position - 1]
    }

    private fun isPositionHeader(position: Int): Boolean {
        return position == 0
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(song: Song, listenerItemClick: (Song) -> Unit) = with(itemView) {

            tvSingerName.text = song.author
            tvSongTitle.text = song.title

            Glide.with(this).load(song.thumbUrl)
                    .apply(RequestOptions().placeholder(R.drawable.no_default_thumbnail).error(R.drawable.no_default_thumbnail))
                    .into(ivAlbumThumbnail)

            //Listener for all view click
            llSongItemContainer.setOnClickListener { listenerItemClick(song) }
        }
    }

    class HeaderViewHoder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(title : String) = with(itemView){
            tvHeaderText.setText(Html.fromHtml(title), TextView.BufferType.SPANNABLE)
        }
    }
}