package com.smess.songsplayer.scenes.preview

import com.smess.basemvpandroidarchitecture.ui.BasePresenter

class PreviewPresenter(viewContract: PreviewContract.ViewContract) : BasePresenter<PreviewContract.ViewContract>(viewContract),
        PreviewContract.PresenterContract {

    companion object {
        private val TAG = PreviewPresenter::class.java.name
    }

    private var mediaCompleted = false

    private var playbackPosition: Int = 0

    override fun onClickGoBack() {
        view.stopPlayer()
        view.goBack()
    }

    override fun onClickPlayPauseButton(isPlaying: Boolean) {
        if (!isPlaying) {
            view.restartPlayer(playbackPosition)
            view.showPauseButton()
        } else {
            view.pausePlayer()
            view.showPlayButton()
        }
    }

    override fun onClickNextSongButton(currentItem: Int, childCount: Int) {
        tryNextSong(currentItem, childCount)
    }

    override fun onClickPreviousSongButton(currentItem: Int) {
        tryPreviousSong(currentItem)
    }

    override fun savePlaybackPosition(currentPosition: Int) {
        playbackPosition = currentPosition
    }

    override fun onMediaCompleted(currentItem: Int, childCount: Int) {
        if (currentItem < childCount - 1) {
            mediaCompleted = true
            view.showNextSong()

        } else {
            view.stopPlayer()
        }
    }

    private fun tryNextSong(currentItem: Int, childCount: Int) {
        if (currentItem < childCount - 1) {
            view.showNextSong()
        }
    }

    private fun tryPreviousSong(currentItem: Int) {
        if (currentItem > 0) {
            view.showPreviousSong()
        }
    }

    override fun onPageChanged(position: Int, size: Int, isPlaying: Boolean) {
        playbackPosition = 0
        if(mediaCompleted){
            view.prepareMediaPlayer(true)
            mediaCompleted = false
        }else {
            view.prepareMediaPlayer(isPlaying)
        }
    }
}