package com.smess.songsplayer.scenes.songThumb


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.smess.songsplayer.R
import kotlinx.android.synthetic.main.item_song_preview.*


class SongThumbFragment : Fragment() {

    private var thumbUrl: String? = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_song_thumb, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        thumbUrl = arguments?.getString(ARG_SONG_URL)
        Glide.with(activity!!).load(thumbUrl).apply(RequestOptions()
                .placeholder(R.drawable.no_default_thumbnail)
                .error(R.drawable.no_default_thumbnail))
                .into(ivSongThumb)
    }


    companion object{
        private val ARG_SONG_URL = "songThumbFragment_arg_song_url"

        fun newInstance(thumbUrl: String):SongThumbFragment{

            val args = Bundle()
            args.putSerializable(ARG_SONG_URL, thumbUrl)
            val fragment = SongThumbFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
