package com.smess.songsplayer.scenes.preview.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.smess.songsplayer.domain.models.Song
import com.smess.songsplayer.scenes.songThumb.SongThumbFragment

class SongPreviewPagerAdapter(fragmentManager: FragmentManager, private val songs: ArrayList<Song>) : FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        return SongThumbFragment.newInstance(songs[position].thumbUrl)
    }

    override fun getCount(): Int {
        return songs.size
    }
}