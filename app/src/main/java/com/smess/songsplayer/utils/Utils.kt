package com.smess.songsplayer.utils

import android.content.Context
import android.net.ConnectivityManager
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.widget.TextView



/**
 * Created by samy.messara
 */
object Utils {

    /**
     * Check if the device has internet connection
     *
     * @param context
     *
     * @return true is network available, else false
     */
    fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }


    /**
     * Display snackbar with message and button
     *
     * @param parentContainer
     * @param error
     * @param buttonText
     */
    fun displayErrorSnackbar(context: Context, parentContainer: CoordinatorLayout, error: String, buttonText: String) {
        val snackBar = Snackbar.make(parentContainer, error, Snackbar.LENGTH_LONG)
        snackBar.setAction(buttonText, { snackBar.dismiss() })
        // Changing button text color
        snackBar.setActionTextColor(ContextCompat.getColor(context, android.R.color.white))

        // Changing message text color
        val sbView = snackBar.view
        val textView = sbView.findViewById(android.support.design.R.id.snackbar_text) as TextView
        textView.setTextColor(ContextCompat.getColor(context, android.R.color.white))
        snackBar.show()
    }

    /**
     * Get string time from millis
     *
     * @param millis
     */
    fun getTimeString(millis: Int): String {
        val buf = StringBuffer()

        val minutes = (millis % (1000 * 60 * 60) / (1000 * 60))
        val seconds = (millis % (1000 * 60 * 60) % (1000 * 60) / 1000)

        buf
                .append(String.format("%01d", minutes))
                .append(":")
                .append(String.format("%02d", seconds))

        return buf.toString()
    }

}