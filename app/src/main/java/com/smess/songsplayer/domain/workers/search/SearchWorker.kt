package com.smess.songsplayer.domain.workers.search

import com.smess.basemvpandroidarchitecture.workers.AbstractWorker
import com.smess.songsplayer.services.search.SearchService

class SearchWorker(private val terms: String, private val callback: SearchWorkerCallback, private val tag: String) : AbstractWorker() {

    override fun run() {
        SearchService(callback, tag).execute(terms)
    }
}