package com.smess.songsplayer.domain.models

import java.io.Serializable

data class Song(val id: Int, val title: String, val author: String, val thumbUrl: String, val previewUrl: String) : Serializable