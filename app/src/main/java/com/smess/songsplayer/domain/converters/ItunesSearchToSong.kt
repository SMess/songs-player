package com.smess.songsplayer.domain.converters

import com.smess.songsplayer.domain.models.ItunesSearch
import com.smess.songsplayer.domain.models.Song

object ItunesSearchToSong {

    fun searchToSongsList(search: ItunesSearch): ArrayList<Song> {
        var songsList: ArrayList<Song> = ArrayList()
        var id = 0
        for (result in search.results) {
            if (!result.trackName.isNullOrBlank() && !result.artistName.isNullOrBlank() && !result.artworkUrl100.isNullOrBlank() && !result.previewUrl.isNullOrBlank()){
                songsList.add(Song(id, result.trackName, result.artistName, result.artworkUrl100.replace("100x100", "300x300"), result.previewUrl))
                id++
            }
        }
        return songsList
    }
}